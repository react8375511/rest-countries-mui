import Cards from './Cards';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import { useNavigate } from 'react-router-dom';


export default function Country({ data }) {

  const navigate=useNavigate();

  const carddet=(uid)=>{
    navigate(`/country/${uid}`);
  }

  return (
    <Box sx={{ width: '90%', margin: 'auto' }}>
      <Grid container spacing={10} justifyContent="center">
        {data.length > 0 ? (
          data.map((country, index) => (
            <Grid item key={index} xs={12} sm={6} md={4} lg={3} onClick={()=>carddet(country.ccn3)}>
              <Cards data={country} />
            </Grid>
          ))
        ) : (
          <Grid item xs={12}>
            <div className='nothing'>No such countries found</div>
          </Grid>
        )}
      </Grid>
    </Box>
  );
}
