import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import { useTheme } from '../themeContext'


export default function Cards({ data }) {
  const { getHeaderBackgroundColor, getTextColor } = useTheme();
  
  return (
    <Card sx={{ height: 320, backgroundColor: getHeaderBackgroundColor() }}>
      <CardActionArea>
        <CardMedia
          component="img"
          height="160"
          image={data.flags.png}
          style={{ objectFit: 'fill' }}
        />
        <CardContent sx={{ height: "8rem" }}>
          <Typography gutterBottom variant="h6" component="div" color={getTextColor()}>
            <span style={{fontWeight:'700'}}>{data.name.official}</span>
          </Typography>
          <Typography variant="body2" color={getTextColor()}>
            Population: <span style={{ color: 'grey' }}>{data.population}</span>
          </Typography>
          <Typography variant="body2" color={getTextColor()}>
            Region: <span style={{ color: 'grey' }}>{data.region}</span>
          </Typography>
          <Typography variant="body2" color={getTextColor()}>
            Capital: <span style={{ color: 'grey' }}>{data.capital}</span>
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}