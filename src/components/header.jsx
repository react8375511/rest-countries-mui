import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { useTheme } from '../themeContext'
import DarkModeIcon from '@mui/icons-material/DarkMode';
import LightModeIcon from '@mui/icons-material/LightMode';



export default function ButtonAppBar() {
  const {isDarkMode,toggleTheme,getHeaderBackgroundColor, getTextColor}=useTheme();



  return (
    <Box sx={{ flexGrow: 1}}>
      <AppBar position="static" sx={{paddingLeft:'7rem',paddingRight:'6rem', backgroundColor: getHeaderBackgroundColor(), color: getTextColor() ,height:'5rem'}}>
      <Toolbar sx={{height:'100%'}}>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 ,fontSize:'2rem',fontWeight:600}}>
            
            Where in the world?
          </Typography>
           {!isDarkMode ? (<DarkModeIcon sx={{ fontSize: 24 }} />) : <LightModeIcon sx={{ fontSize: 24 }} />}
          <Button color="inherit" onClick={toggleTheme}>{(!isDarkMode)?"Dark":"Light"} Mode</Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
