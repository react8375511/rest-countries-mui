import * as React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Stack from '@mui/material/Stack';
import { useState } from 'react';
import SearchIcon from '@mui/icons-material/Search';
import { useTheme } from '../themeContext'
import { Paper,InputBase } from '@mui/material';
import { createTheme } from '@mui/material/styles';
import { ThemeProvider } from '@mui/material';

export default function Filter({ searchCountry, currentSelectedRegion, currentSelectedSubRegion, subRegionOptions, handleSort }) {
  const regionOptions = ['Filter by Region', 'Africa', 'Americas', 'Asia', 'Europe', 'Oceania'];

  const [name, setName] = useState('');
  const [selectedOption, setSelectedOption] = useState('Filter by Region');
  const [selectedRegion, setSelectedRegion] = useState({ region: 'Filter by Region', subRegion: 'Filter by Sub-Region' });
  const [selectedSubRegion, setSelectedSubRegion] = useState('Filter by Sub-Region');

  const [sortValue, setSortValue] = useState('');

  const {isDarkMode,toggleTheme,getHeaderBackgroundColor, getTextColor}=useTheme();

  const handleSearch = (event) => {
    setName(event.target.value);
    searchCountry(event);
  };

  const handleFilterSelect = (filter) => {
    setSelectedOption(filter);
    setSelectedRegion({ region: filter, subRegion: 'Filter by Sub-Region' });
    currentSelectedRegion(filter);
  };

  const handleFilterSelectSubRegion = (event) => {
    const filter = event.target.value;
    setSelectedRegion({ ...selectedRegion, subRegion: filter });
    setSelectedSubRegion(filter);
    currentSelectedSubRegion(filter);
  };

  const handleSortChange = (event) => {
    setSortValue(event.target.value);
    const [sortType, sortby] = event.target.value.split('-');
    handleSort(sortby, sortType);
  };



  const darkTheme = createTheme({
    palette: {
      mode: isDarkMode ? "dark" : "light",
      background: {
        paper: isDarkMode ? "hsl(207, 26%, 17%)" : "#fff",
        default: isDarkMode ? "hsl(207, 26%, 17%)" : "#fff",
      },  
    },
  });

  return (
  <ThemeProvider theme={darkTheme}>
    <Box
      component="form"
      sx={{
        '& > :not(style)': { m: 4 },
        width: '94.5%',
        margin: 'auto',
      }}
      noValidate
      autoComplete="off"
    >
      <Stack margin="auto" width="90%" direction="row" justifyContent="space-between" alignItems="center">
          
            
            <Paper
              sx={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                width: '27rem',
                height: '3.5rem',
                // backgroundColor: getHeaderBackgroundColor(),
                // color: getTextColor(),
              }}
            >
              <SearchIcon sx={{color:"grey"}} />
              <InputBase
                sx={{
                  width: '80%',
                  marginLeft: '2rem',
                  color: getTextColor()
                }}
                placeholder="Search a country"
                value={name}
                onChange={handleSearch}  
              />
            </Paper>        

        
        <FormControl variant="outlined" sx={{ width: 220 }}>
          <InputLabel
            sx={{ backgroundColor: getHeaderBackgroundColor(), color: getTextColor() }}
          >Sort by</InputLabel>
          <Select
            labelId="sort-label"
            id="sort-select"
            value={sortValue}
            onChange={handleSortChange}
            sx={{ backgroundColor: getHeaderBackgroundColor(), color: getTextColor() }}

          >
            <MenuItem value="population-asc">Population (Ascending)</MenuItem>
            <MenuItem value="population-desc">Population (Descending)</MenuItem>
            <MenuItem value="area-asc">Area (Ascending)</MenuItem>
            <MenuItem value="area-desc">Area (Descending)</MenuItem>
          </Select>
        </FormControl>



        <FormControl variant="outlined" sx={{ width: 245 }  }>
          <Select
            labelId="sub-region-label"
            id="sub-region-select"
            value={selectedSubRegion}
            onChange={handleFilterSelectSubRegion}
            sx={{ backgroundColor: getHeaderBackgroundColor(), color: getTextColor() }}
            MenuProps={{
              PaperProps: {
                style: {
                  maxHeight: '16rem',
                  // position: 'absolute',
                }
              }
            }}
          >
            {subRegionOptions[selectedRegion.region] &&
              subRegionOptions[selectedRegion.region].map((option) => (
                <MenuItem key={option} value={option}>
                  {option}
                </MenuItem>
              ))}
          </Select>
        </FormControl>  



        <FormControl sx={{ m: 1, width: 180 }}>
          <Select
            labelId="region-label"
            id="region-select"
            value={selectedOption}
            onChange={(event) => handleFilterSelect(event.target.value)}
            sx={{ backgroundColor: getHeaderBackgroundColor(), color: getTextColor() }}
          >
            {regionOptions.map((option) => (
              <MenuItem key={option} value={option} >
                {option}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Stack>
    </Box>

    </ThemeProvider>
  );
}
