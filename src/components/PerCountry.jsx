import React, { useEffect, useState } from 'react';
import { useTheme } from '../themeContext';
import { useNavigate, useParams } from 'react-router-dom';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Typography from '@mui/material/Typography';

export default function CountryDetails() {
  const [perdata, setPerdata] = useState({});
  const [err, setErr] = useState(true);
  const [countryname, setCountryname] = useState({});
  const { isDarkMode } = useTheme();
  const navigate = useNavigate();
  const { uid } = useParams();

  function back() {
    navigate('/');
  }

  useEffect(() => {
    async function getData() {
      try {
        const response = await fetch(`https://restcountries.com/v3.1/alpha/${uid}`);
        const response2 = await fetch("https://restcountries.com/v3.1/all");
        if (!response.ok) {
          throw new Error('Failed to fetch data');
        }

        const alldata = await response2.json();
        const perCData = await response.json();
        setPerdata(perCData[0]);

        setCountryname(alldata.reduce((acc, curr) => {
          const cname = curr.cca3;
          if (!acc[cname]) {
            acc[cname] = [];
          }
          acc[cname].push(curr.name.common);
          return acc;
        }, {}));

      } catch (error) {
        console.error('Error fetching data:', error);
        setErr(false);
      }
    }

    getData();
  }, [uid]);

  return (
    <>
      {err && perdata &&
        <Box sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between', padding: '5rem' }}>
          <Paper onClick={back} sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', width: '10%', height: '4rem' }}>
            <ArrowBackIcon />
            <p style={{ color: 'black', fontWeight: 500 }}>Back</p>
          </Paper>

          <Box sx={{ display: 'flex', justifyContent: 'space-between', paddingTop: '4rem', height: '20rem' }}>
            <Box sx={{ width: '40%' }}>
              {perdata.flags && <img style={{ height: '100%', width: '100%' }} src={perdata.flags.png} alt="" />}
            </Box>

            <Box sx={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between', width: '54%', padding: '2rem' }}>
              {perdata.name &&
                <Typography variant="h3" style={{ color: 'black', fontWeight: 500 }}>{perdata.name.common}</Typography>
              }

              <Box sx={{ width: '95%', display: 'flex' }}>
                <Box sx={{ width: '50%', display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }}>
                  <p style={{ color: 'black', fontWeight: 500 }}>Native Name: {perdata.altSpellings && <span style={{ color: 'grey' }}>{perdata.altSpellings[1]}</span>}</p>
                  <p style={{ color: 'black', fontWeight: 500 }}>Population : {perdata.population && <span style={{ color: 'grey' }}>{perdata.population.toLocaleString()}</span>}</p>
                  <p style={{ color: 'black', fontWeight: 500 }}>Region : {perdata.region && <span style={{ color: 'grey' }}>{perdata.region}</span>}</p>
                  <p style={{ color: 'black', fontWeight: 500 }}>Sub Region: {perdata.subregion && <span style={{ color: 'grey' }}>{perdata.subregion}</span>}</p>
                  <p style={{ color: 'black', fontWeight: 500 }}>Capital : {perdata.capital && <span style={{ color: 'grey' }}>{perdata.capital}</span>}</p>
                </Box>
                <Box sx={{ width: '50%', display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }}>
                  <p style={{ color: 'black', fontWeight: 500 }}>Top Level Domain : {perdata.tld && <span style={{ color: 'grey' }}>{perdata.tld}</span>}</p>
                  <p style={{ color: 'black', fontWeight: 500 }}>Currencies: {perdata.currencies &&
                    <span style={{ color: 'grey' }}>
                      {Object.values(perdata.currencies).map(currency => currency.name).join(', ')}
                    </span>
                  }</p>
                  <p style={{ color: 'black', fontWeight: 500 }}>Languages : {perdata.languages && (
                    <span style={{ color: 'grey' }}>
                      {Object.entries(perdata.languages).map(([key, value]) => (
                        <span className='border' key={key}>
                          {value}
                          {key !== Object.keys(perdata.languages)[Object.keys(perdata.languages).length - 1] && ", "}
                        </span>
                      ))}
                    </span>
                  )}
                  </p>
                </Box>
              </Box>
              <Box sx={{ display: 'flex', alignItems: 'center' }}>
                <p style={{ color: 'black', fontWeight: 500, width: '22%' }}> Border Countries :</p>
                <span style={{ display: 'flex', flexWrap: 'wrap', marginLeft: '0.7rem', maxWidth: 'max-content', gap: '0.5rem', width: '80%', height: '2rem' }}>
                  {perdata.borders ? perdata.borders.map((border, index) => (
                    (<span style={{ backgroundColor: isDarkMode ? 'hsl(209, 23%, 22%)' : 'white', height: '3rem', width: '8rem', display: 'flex', justifyContent: 'center', alignItems: 'center', boxShadow: 'rgba(99, 99, 99, 0.2) 0px 2px 8px 0px', cursor: 'pointer' }} key={index}>
                      {countryname[border]}
                    </span>)
                  ))
                    :
                    (<strong>No border Countries</strong>)
                  }
                </span>
              </Box>
            </Box>
          </Box>
        </Box>
      }
    </>
  );
}
