import React, { createContext, useState, useContext } from 'react';

const ThemeContext = createContext();

export const useTheme = () => useContext(ThemeContext);

export const ThemeProvider = ({ children }) => {
  const [isDarkMode, setIsDarkMode] = useState(false);

  const toggleTheme = () => {
    setIsDarkMode(prevTheme => !prevTheme);
  };

  const getHeaderBackgroundColor = () => {
    return isDarkMode ? 'hsl(209, 23%, 22%)' : 'white';
  };

  const getTextColor = () => {
    return isDarkMode ? 'white' : 'black';
  };

  return (
    <ThemeContext.Provider value={{ isDarkMode, toggleTheme, getHeaderBackgroundColor, getTextColor }}>
      {children}
    </ThemeContext.Provider>
  );
};
