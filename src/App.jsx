import { React, useEffect, useState } from 'react'

import Header from './components/header'
import Filter from './components/Filter'
import Country from './components/Country'
import PerCountry from './components/PerCountry'
import Spin from './components/Spin'
import { useTheme } from './themeContext'
import {Route , Routes,useNavigate} from 'react-router-dom'


function App() {
  const [data, setData] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [cregion, setCregion] = useState("Filter by Region");
  const [subregion,setSubregion]=useState("Filter by Sub-Region");
  
  const [sortby, setSortBy] = useState('');
  const [sortType, setSortType] = useState('');


  useEffect(() => {
    async function getData() {
      setLoading(true);
      try {
        const response = await fetch("https://restcountries.com/v3.1/all");
        if (!response.ok) {
          throw new Error('Failed to fetch data');
        }
        const dd = await response.json();


        setData(dd);
        setFilteredData(dd);
        setLoading(false);
      } catch (error) {
        console.error('Error fetching data:', error);
        setLoading(false);
      }
    }

    getData();
  }, []);
  const subRegionOption = data.reduce((acc, country) => {
    
    if (country.subregion) {
      if (!acc["Filter by Region"]) {
        acc["Filter by Region"]=  ["Filter by Sub-Region"];
      }
      if(!acc["Filter by Region"].includes(country.subregion)) {
        acc["Filter by Region"].push(country.subregion);
      }
      if (!acc[country.region]) {
        acc[country.region] = ["Filter by Sub-Region"];
      }
      if (!acc[country.region].includes(country.subregion)) {
        acc[country.region].push(country.subregion);
      }
    }
    return acc;
  }, {});


  function onsearched(event) {
    const typed = event.target.value;
    if(typed==''){
      selectedFilter(data);
    }
    // console.log(filteredData)
    // console.log(mixed, (mixed.toLowerCase()== 'Filter by Region'))
    const filteredCountries = data.filter(country => {
      // console.log(country.name.official);
      // if(sorted=='populatio')
      return ((cregion.toLowerCase() == 'Filter by Region'.toLowerCase()) || (cregion.toLowerCase() == country.region.toLowerCase()))
      &&(subregion.toLowerCase()=='Filter by Sub-Region'.toLowerCase() || (country.subregion!=undefined && country.subregion.toLowerCase().includes(subregion.toLowerCase())))
       && country.name.official.toLowerCase().includes(typed.toLowerCase())
    });

    onSort(filteredCountries,sortType,sortby);
  }


  function selectedFilter(reg) {
    setCregion(reg.toLowerCase());
    if (reg === "Filter by Region") {
      setFilteredData(data);
    }
    else {
      const filteredCountries = data.filter(country => {
        // console.log(country.region, reg, typeof reg);
        return country.region.toLowerCase().includes(reg.toLowerCase())
      });

      setFilteredData(filteredCountries);
    }

  }

  function selectedSubRegion(subreg) {
    setSubregion(subreg.toLowerCase());

    if (subreg === "Filter by Sub-Region") {

      if (cregion === "Filter by Region") {
        setFilteredData(data);
      } 
      else {
        const filteredCountries = data.filter(country => {
          return country.region.toLowerCase() === cregion.toLowerCase();
        });
        setFilteredData(filteredCountries);
      }
    } 
    else {
      const filteredCountries = data.filter(country => {
        if (country.subregion == null) return null;
        return (
          (cregion.toLowerCase() === "filter by region" ||
            country.region.toLowerCase() === cregion.toLowerCase()) &&
          country.subregion.toLowerCase() === subreg.toLowerCase()
        );
      });
      setFilteredData(filteredCountries);
    }

  }


  function onSort(data,sortType,sortby){
    if (sortby === 'population') {
      if (sortType === 'asc') {
        data.sort((a, b) => a.population - b.population);
      } else if (sortType === 'desc') {
        data.sort((a, b) => b.population - a.population);
      }
    }
    else if (sortby === 'area') {
      data.sort((a, b) => {
        if (sortType === 'asc') {
          return a.area - b.area;
        } else {
          return b.area - a.area;
        }
      });
    }
    setFilteredData(data);
  }


  function handleSort(sortType, sortby) {
    setSortBy(sortby);
    setSortType(sortType);
    onSort(filteredData,sortType,sortby);
  }

  const {isDarkMode, getTextColor}=useTheme();


  return (
    <Routes>
        <Route path='/' element={

          <section style={{ backgroundColor: isDarkMode ? 'hsl(207, 26%, 17%)' : 'white', color: getTextColor(),minHeight:'100vh', minWidth:'100vw'}}>
              <header>
                <Header/>
              </header>
              <section >
              <Filter searchCountry={onsearched}
                    currentSelectedRegion={selectedFilter}
                    currentSelectedSubRegion={selectedSubRegion}
                    subRegionOptions={subRegionOption}
                    handleSort={handleSort}
                  />
              </section>
              
              {loading && <Spin />}
              {loading || data && <Country data={filteredData}/>}
          </section>


        }></Route>
        <Route path="/country/:uid" element={
          
            <section className={`container ${isDarkMode ? "dark-container" : ""}`}>
              <Header/>
              <PerCountry/>
            </section>
      }></Route>
      </Routes>
  )
}

export default App
